## Horizon - Chatbot Order Status

The participants are required to clone this repository and create a private Gitlab repository under their own username (Single repository per team). The following created sections in this README.md need to be duly filled, highlighting the denoted points for the solution/implementation. Please feel free to create further sub-sections in this markdown, the idea is to understand the gist of the components in a singular document.

### Project Overview
----------------------------------

A brief description of 
Develop a order status chatbot.
Chatbot using Dialogflow.

### Solution Description
----------------------------------

#### Architecture Diagram

Affix an image of the flow diagram/architecture diagram of the solution

#### Technical Description

An overview of 
html
bootstrap
Node js
java script

### Team Members
----------------------------------

List of team member names and email IDs with their contributions
Ashutosh Singh - b217014@iiit-bh.ac.in
Yash Dwivedi - B317045@iiit-bh.ac.in
Nikhitha Poludasu - B317026@iiit-bh.ac.in
Shristy Srivastava - B317048@iiit-bh.ac.in